# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170209044406) do

  create_table "enterprise_emails", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enterprise_phones", force: :cascade do |t|
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enterprises", force: :cascade do |t|
    t.string   "name"
    t.string   "cnpj"
    t.string   "sequence"
    t.string   "street"
    t.integer  "number"
    t.string   "district"
    t.string   "city"
    t.string   "state"
    t.string   "postCode"
    t.string   "country"
    t.string   "website"
    t.datetime "foudation"
    t.integer  "enterpriseEmail_id"
    t.integer  "enterprisePhone_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "enterprises", ["enterpriseEmail_id"], name: "index_enterprises_on_enterpriseEmail_id"
  add_index "enterprises", ["enterprisePhone_id"], name: "index_enterprises_on_enterprisePhone_id"

end

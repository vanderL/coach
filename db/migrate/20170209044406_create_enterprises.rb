class CreateEnterprises < ActiveRecord::Migration
  def change
    create_table :enterprises do |t|
      t.string :name
      t.string :cnpj
      t.string :sequence
      t.string :street
      t.integer :number
      t.string :district
      t.string :city
      t.string :state
      t.string :postCode
      t.string :country
      t.string :website
      t.datetime :foudation
      t.references :enterpriseEmail, index: true, foreign_key: true
      t.references :enterprisePhone, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.boolean :active
      t.references :enterprise, index: true, foreign_key: true
      t.datetime :expiry
      t.string :role
      t.references :modality, index: true, foreign_key: true
      t.string :pay
      t.string :workplace
      t.string :workload
      t.integer :numberJobs
      t.text :benefits
      t.text :required
      t.text :wanted
      t.text :goings
      t.string :wageClaim

      t.timestamps null: false
    end
  end
end

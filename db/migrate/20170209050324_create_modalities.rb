class CreateModalities < ActiveRecord::Migration
  def change
    create_table :modalities do |t|
      t.string :modality

      t.timestamps null: false
    end
  end
end

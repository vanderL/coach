class CreateEnterprisePhones < ActiveRecord::Migration
  def change
    create_table :enterprise_phones do |t|
      t.string :phone

      t.timestamps null: false
    end
  end
end

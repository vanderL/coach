# COACH

**Local Server on port 3000**
```sh
Clone the package
```
```sh
Run 'bundle install' (use sudo if needed) to install the dependencies
```
```sh
Run 'rails server' to start the server, live reload and watchers and start developing.
```

### Requirements

- Ruby 2.3
- Rails 4.2.6 
```sh
Run 'gem install rails 4.2.6' for install
```